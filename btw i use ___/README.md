## If your distro of choice isn't listed, send me a message on [Telegram](https://t.me/Amolith), a toot on [Mastodon](https://fosstodon.org/@amolith), or an [email](mailto:amolith@nixnet.xyz) encrypted with my [GPG key](https://nixnet.xyz/amolith.txt).

### Ordered Todo List
- OpenBSD
- deepin
- Linux Mint
- MX Linux
- ArchLabs
- Ubuntu flavours
- Neon
- openSUSE
- Peppermint

All logos are individually licensed and belong to the respective distro. Below, I have added the licensing information for each and links to furthur resources.

- [Arch](#arch)
- [Debian](#debian)
- [elementary](#elementary)
- [Fedora](#fedora)
- [Gentoo](#gentoo)
- [Manjaro](#manjaro)
- [NixOS](#nixos)
- [Solus](#solus)
- [Ubuntu](#ubuntu)
- [Todo List](#todo-list)

## Arch
Wallpapers with the Arch logo are licensed under [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) in compliance with the [Arch Wiki](https://wiki.archlinux.org/index.php/DeveloperWiki:TrademarkPolicy#Attribution).
## Debian
> The Debian Open Use Logo(s) are Copyright (c) 1999 Software in the Public Interest, Inc., and are released under the terms of the GNU Lesser General Public License, version 3 or any later version, or, at your option, of the Creative Commons Attribution-ShareAlike 3.0 Unported License.

I'll just leave it like this ☝️
## elementary
The elementary logo is a trademark of elementary LLC. However, permission is not required to use it: you can look at their [Brand](https://elementary.io/brand) page for more information.
## Fedora
The Fedora logo is a trademark of Red Hat, Inc. and is used with permission. Because it is a trademark, it has been removed from the source SVG. If you need it, read over their [usage guidelines](https://fedoraproject.org/wiki/Logo/UsageGuidelines) before sending an email request to their logo queue for a properly licensed copy.
## Gentoo
The Gentoo logo is a trademark of the Gentoo Foundation, Inc. and the license is based on the [Django Trademark License Agreement](https://www.djangoproject.com/trademarks/) and is, as such, also released under the [CC-BY-SA 3.0 License](http://creativecommons.org/licenses/by-sa/3.0/). You can read more about Gentoo's logo guidelines [here](https://www.gentoo.org/inside-gentoo/foundation/name-logo-guidelines.html).
## Kali
I couldn't find logo/brand-*specific* licensing or trademark information so I'll just say that it's an unregistered trademark (hence "TM") of Offensive Security.
## Manjaro
I couldn't find anything about Majaro's license other than this:
> Manjaro Linux, copyright © 2011-2018 Philip Müller and the Manjaro Developers

I got the svg from their [GitLab instance](https://gitlab.manjaro.org/artwork/branding/logo/blob/master/logo.svg).
## NixOS
I couldn't find much about the NixOS logo so I'm going to assume it's under the MIT/X11 like the rest of the OS, stated at the bottom of [this page](https://nixos.org/nixos/about.html). 
## Solus
Solus Logo: Copyright © 2016-2017 Solus Project, Ikey Doherty.

(copied from Solus' [Branding page](https://getsol.us/branding/))
## Ubuntu
**Note:** I have not yet recieved permission from Canonical for using the Ubuntu logo.

The Ubuntu logo is intellectual property owned by Canonical and is subject to their [IPRights Policy](https://www.ubuntu.com/legal/terms-and-policies/intellectual-property-policy).
